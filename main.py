import pandas as pd
import pyspark
from pyspark.sql.types import *
# from iwzudfs import  *
from pyspark.sql import functions as F
import iwzconstants
import datetime as dt
import os

spark = pyspark.sql.SparkSession.builder.appName('iwztest2').getOrCreate()

@F.udf('double')
def total_volume(vols):
    return sum(vols)

@F.udf('double')
def average_occ(occs, lane_cnt):
    return sum(occs)/lane_cnt

@F.udf('double')
def avg_speed(vols,speeds):
    if sum(vols) == 0: return 0
    return (sum([v*s for v,s in zip(vols,speeds)]) / sum(vols))*0.6214


@F.udf('double')
def delay_min(seg_len, speed, speed_limit):
    d = ((seg_len/speed)-(seg_len/speed_limit))*60
    return 0 if d < 0 else d


def peak_hr_and_vol(df):
    tmp = df.filter((F.col('dayofweek') >= 1) & (F.col('dayofweek') <= 6)). \
        withColumn('only_time', F.to_timestamp(F.date_format(F.col('time'), 'HH:mm:ss'), 'HH:mm:ss')). \
        groupBy('NameDirection', 'week', F.window('only_time', '1 hour', '1 minute')).agg(
        F.sum('total_vol').alias('tv'))

    tmp = tmp.withColumn('rn', F.row_number().over(pyspark.sql.Window.partitionBy('NameDirection', 'week'). \
                                                   orderBy(F.col('tv').desc()))).where(F.col('rn') == 1)
    return tmp.select('NameDirection', 'week', F.date_format(F.col('window.start'), 'HH:mm:ss').alias('peak_start'),
                      (F.col('tv') / 60).alias('peak_hr_vol'))

npercentile = lambda col,per: F.expr('percentile_approx({}, {})'.format(col,per))

median_percentile = lambda col : npercentile(col,0.5)

avg_vph =  lambda s,e :F.sum(F.when((F.col('dayofweek') >= 1) & (F.col('dayofweek') <= 6) \
                                    & (F.hour(F.col('time')) >= s) & (F.hour(F.col('time')) <= e ),\
                                    F.col('total_vol') ).otherwise(0)) /((e-s) * 60)


### Read Data

cols = []
for col_name in ['sensor','date','start','end','status']:
    cols.append(StructField(col_name,StringType(),True))
cols.append(StructField('lanes',IntegerType(),True))
for i in range(1,6):
    for lane_col_name in ['{}_{}'.format(x, i) for x in ['laneId','lane_count', 'lane_vol', 'lane_occ', 'lane_speed','lane_small_cnt',
                          'lane_small_vol','lane_med_cnt','lane_med_vol','lane_large_cnt','lane_large_vol']]:
        cols.append(StructField(lane_col_name,DoubleType(),True))

schema = StructType(cols)

# tf = spark.createDataFrame([],schema=schema)

start = dt.date(2018,5,27)
end = dt.date(2018,6,30)
files = []
#while start <= end:
 #   files.append('hdfs://master.intrans.iastate.edu:8020/user/team/WAVETRONIX/IOWA/2018/{}/{}.txt'\
 #                                .format(start.strftime('%Y%m'), start.strftime('%m%d%Y')))
    # print(start.strftime('%m%d%Y'))
  #  start += dt.timedelta(1)

files.append('/reactor/WAVETRONIX/IOWA/2018/201805/05{2[7-9],3[0-1]}*.txt')
files.append('/reactor/WAVETRONIX/IOWA/2018/201806/*.txt')
tf = spark.read.format('csv').option('header','true').schema(schema).load(files)
#tf.persist(pyspark.StorageLevel.MEMORY_AND_DISK)
#tf = tf.repartition(3000)
#tf = spark.read.csv(files, header=True, schema=schema)

# sensors = pd.read_csv('sensorlist.csv')
sensors = spark.read.csv('/reactor/iwz/sensorlist.csv',header=True,inferSchema=True).toPandas()
sensor_names = set(sensors['Sensor'])
sdf = spark.createDataFrame(sensors)
sdf =sdf.withColumnRenamed('Sensor','sensor')

#event_alerts = pd.read_csv('EventLog.csv')
event_alerts = spark.read.csv('/reactor/iwz/EventLog.csv',header=True,inferSchema=True).toPandas()
event_alerts['startTimeEvent_fm'] = pd.to_datetime(event_alerts['startTimeEvent_fm'])
event_alerts['endTimeEvent_fm'] = pd.to_datetime(event_alerts['endTimeEvent_fm'])
eadf = spark.createDataFrame(event_alerts)


#### Data Preprocessing
tdf = tf.filter((F.col('status') == 'operational') & (F.col('lanes') > 0) & (F.col('sensor').isin(sensor_names)))

vol_cols = [x for x in tdf.columns if x.startswith('lane_vol')]
occ_cols = [x for x in tdf.columns if x.startswith('lane_occ')]
speed_cols = [x for x in tdf.columns if x.startswith('lane_speed')]

tdf = tdf.fillna(0, subset=vol_cols+occ_cols+speed_cols)

tdf = tdf.withColumn('total_vol', total_volume(F.array(*vol_cols))).withColumn('avg_occ', average_occ(F.array(*occ_cols),'lanes')).withColumn('avg_speed', avg_speed(F.array(*vol_cols),F.array(*speed_cols)))

tdf = tdf.select('sensor',
           F.to_timestamp(F.concat(F.col('date'), F.lit(' '), F.substring(F.col('start'),1,4)), 'yyyyMMdd HHmm').alias('time'),
          'lanes','total_vol','avg_occ','avg_speed')

tdf = tdf.join(F.broadcast(sdf),'sensor')

gdf = tdf.groupBy('sensor','time','NameDirection','coded_direction','ID','Latitude','Longitude','Length','SPEED_LIMIT_x').agg(F.max('lanes').alias('lanes'),
                                     F.sum('total_vol').alias('total_vol'),
                                     F.avg('avg_occ').alias('avg_occ'),
                                     F.avg('avg_speed').alias('avg_speed')
                                    )

#### Performance measure -1

# Run this for each work zone

wz = sensors['NameDirection'][0]

# odf = gdf.filter(F.col('NameDirection') == wz)
odf = gdf.filter((F.col('avg_speed') > 0) & (F.col('total_vol') > 0) & (F.col('total_vol') < 500))
odf = odf.withColumn('delay_min',delay_min(F.col('Length'), F.col('avg_speed'),F.col('SPEED_LIMIT_x')))
odf = odf.withColumn('vehical_min',F.col('delay_min')*F.col('total_vol'))
# One minute data aggregation for all sensors in a particular workzone group.
tvdf = odf.groupBy('NameDirection','time').agg(median_percentile('total_vol').alias('median_vol'),
                                              F.max('total_vol').alias('max_vol'),
                                              F.sum('total_vol').alias('total_vol'),
                                              F.sum('vehical_min').alias('total_vehical_min'),
                                              F.sum('delay_min').alias('total_delay_min'))
# Week is considered from Sun - Sat
tvdf = tvdf.withColumn('day', F.dayofyear('time'))\
        .withColumn('week',F.weekofyear('time')+ (F.dayofweek('time') == 1).cast(IntegerType()))\
        .withColumn('date',F.to_date('time')) \
        .withColumn('dayofweek', F.dayofweek('time'))

peak_df = peak_hr_and_vol(tvdf)

# Whole week aggregation data for all sensors in a particular workzone group
ndf = tvdf.groupBy('NameDirection', 'week').agg(F.min('date').alias('start_date'),
                                          F.max('date').alias('end_date'),
                                          F.countDistinct('day').alias('unique_days'),
                                          F.sum('median_vol').alias('median_vol'),
                                          avg_vph(6,9).alias('am_vph'),
                                          avg_vph(15,18).alias('pm_vph'),
                                          F.sum('total_vehical_min').alias('total_vehical_min'),
                                          F.max('total_vehical_min').alias('max_vehical_min'),
                                          npercentile('total_delay_min',0.95).alias('95_per_total_delay_min'),
                                          F.max('total_delay_min').alias('max_total_delay_min'),
                                          F.mean('total_delay_min').alias('mean_total_delay_min')
)

ndf = ndf.join(peak_df, ['NameDirection','week'], 'left_outer')

#Vamsi test
odf.persist(pyspark.StorageLevel.MEMORY_AND_DISK)
ndf.persist(pyspark.StorageLevel.MEMORY_AND_DISK)

### Performance metrics 2

# edf = eadf.filter(F.col('workzoneName') == wz)
tmpdf = odf.join(F.broadcast(eadf), (odf['NameDirection'] == eadf['workzoneName']) & (odf['time'] >= eadf['startTimeEvent_fm']) & (odf['time'] <= eadf['endTimeEvent_fm']))
tmpdf = tmpdf.filter((F.col('total_vol') > 0) & (F.col('total_vol') < 500) & (F.col('avg_speed') > 0) & (F.col('avg_speed') <45 ))
# Group by all sensors in one minute data interval and find aggregations
cdf = tmpdf.groupBy('NameDirection','time','eventID').agg(F.sum('Length').alias('total_seg_length'),
                                               F.sum('avg_occ').alias('total_avg_occ'),
                                               F.max('total_vol').alias('max_total_vol'),
                                               F.sum('delay_min').alias('total_delay_min'),
                                               F.sum('vehical_min').alias('total_vehical_min'))

cdf = cdf.withColumn('day', F.dayofyear('time')).withColumn('week', F.weekofyear('time')).withColumn('month', F.month('time')).withColumn('year', F.year('time'))

cdf = cdf.withColumn('cond_max_total_vol', F.when(F.col('total_delay_min') > 5, F.col('max_total_vol')).otherwise(0))

gbdf = cdf.groupBy('NameDirection','eventID','year','month','week','day')\
    .agg(F.min('time').alias('min_time'),
         F.max('time').alias('max_time'),
         # TODO time lambda
         (F.round((F.unix_timestamp(F.max('time'),'yyyy-MM-dd HH:mm:ss') - F.unix_timestamp(F.min('time'),'yyyy-MM-dd HH:mm:ss'))/60)+1).alias('total_mins'),
         F.max('total_seg_length').alias('max_total_seg_length'),
         F.mean('total_seg_length').alias('mean_total_seg_length'),
         F.sum('total_avg_occ').alias('total_avg_occ'),
         F.sum('max_total_vol').alias('total_max_vol'),
         F.sum('cond_max_total_vol').alias('total_cond_max_total_vol'),
         F.sum('total_vehical_min').alias('total_vehical_min'),
         F.mean('total_vehical_min').alias('mean_vehical_min'),
         npercentile('total_delay_min', 0.95).alias('95_per_total_delay_min')
        )

# Filter events with the total duration morethan 5 mins
gbdf = gbdf.filter(F.col('total_mins') > 5)

gbdf = gbdf.withColumn('TW_Q_len', F.col('mean_total_seg_length') * F.col('total_mins'))

gbdf = gbdf.withColumn('daytime_event', ((F.hour('min_time') >= 6) & (F.hour('max_time') <= 18)).cast(IntegerType()))

wgdbf = gbdf.groupBy('NameDirection','week').agg(F.sum('daytime_event').alias('daytime_events'),
                                                 F.countDistinct('day').alias('wk_unique_days'),
                                                 F.sum('total_vehical_min').alias('wk_total_vehical_min'),
                                                 F.max('total_vehical_min').alias('max_total_vehical_min'),
                                                 F.count('eventID').alias('eventIds'),
                                                 F.sum('total_max_vol').alias('total_max_vol'),
                                                 F.sum('total_mins').alias('total_mins'),
                                                 F.mean('total_mins').alias('mean_total_mins'),
                                                 npercentile('total_mins', 0.5).alias('median_total_mins'),
                                                 F.max('total_mins').alias('max_total_mins'),
                                                 F.sum('TW_Q_len').alias('total_TW_Q_len'),
                                                 F.max('max_total_seg_length').alias('max_total_seg_length'),
                                                 F.mean('max_total_seg_length').alias('mean_total_seg_length'),
                                                 npercentile('max_total_seg_length',0.5).alias('median_total_seg_length'),
                                                 F.sum(F.when(F.col('max_total_seg_length') > 1, F.col('max_total_seg_length')).otherwise(0)).alias('cond_total_seg_length'),
                                                 npercentile('95_per_total_delay_min',0.95).alias('wk_95_per_total_delay_min')
                                                )


fwpdf = ndf.join(wgdbf,['NameDirection','week'],'outer')


# End of wz group for loop
# -- Outside for
fwpdf = fwpdf.withColumn('avg_delay_per_veh' , F.col('wk_total_vehical_min') / F.col('total_max_vol'))
fwpdf = fwpdf.withColumn('P_veh_in_Q', F.col('total_max_vol')/ F.col('median_vol'))
fwpdf = fwpdf.withColumn('TW_avg_Q',F.col('total_TW_Q_len')/ F.col('total_mins'))
fwpdf = fwpdf.withColumn('P_time_in_Q',F.col('total_mins')/(F.col('unique_days')*24*60))
fwpdf = fwpdf.withColumn('total_delay_perday', F.col('total_vehical_min')/ F.col('unique_days'))
fwpdf =fwpdf.withColumn('total_delay_per_vehicle', F.col('total_vehical_min')/ F.col('median_vol'))
fwpdf =fwpdf.withColumn('P_delay_in_Q', F.col('wk_total_vehical_min')/ F.col('total_vehical_min'))
fwpdf = fwpdf.withColumn('Direction', F.when(F.col('NameDirection').endswith('NB'),1).when(F.col('NameDirection').endswith('EB'),1).otherwise(2))

for col_name in iwzconstants.column_mapping:
    fwpdf = fwpdf.withColumnRenamed(col_name, iwzconstants.column_mapping[col_name])



vdf = fwpdf.select(iwzconstants.sel_cols)

vdf = vdf.withColumn('Direction Name',F.substring_index(F.col('Direction Name'),'-',1) )

vdf = vdf.fillna(0)

vdf.coalesce(1).write.csv('/reactor/iwz/{}'.format(end.strftime('%Y%m%d')), mode='append', header=True)
