from pyspark.sql import functions as F


@F.udf('double')
def total_volume(vols):
    return sum(vols)

@F.udf('double')
def average_occ(occs, lane_cnt):
    return sum(occs)/lane_cnt

@F.udf('double')
def avg_speed(vols,speeds):
    return (sum([v*s for v,s in zip(vols,speeds)]) / sum(vols))*0.6214


@F.udf('double')
def delay_min(seg_len, speed, speed_limit):
    d = ((seg_len/speed)-(seg_len/speed_limit))*60
    return 0 if d < 0 else d

npercentile = lambda col,per: F.expr('percentile_approx({}, {})'.format(col,per))

median_percentile = lambda col : npercentile(col,0.5)

